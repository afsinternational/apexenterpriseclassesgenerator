public interface IObjectPluralSelector extends fflib_ISObjectSelector 
{
	List<objectAPIname> selectById(Set<Id> idSet);
}