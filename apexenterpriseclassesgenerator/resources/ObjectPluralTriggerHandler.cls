public class ObjectPluralTriggerHandler extends AFS_SObjectDomain
{
	public ObjectPluralTriggerHandler(List<objectAPIname> sObjectList)
	{
		// Domain classes are initialised with lists to enforce bulkification throughout
		super(sObjectList);
	}

	//OVERRIDEABLE METHODS
	public override void onBeforeInsert(){

	}

	// PROCESSING BACKGROUND METHODS

	// CONSTRUCTOR

	public class Constructor implements fflib_SObjectDomain.IConstructable
	{
		public fflib_SObjectDomain construct(List<SObject> sObjectList)
		{
			return new ObjectPluralTriggerHandler(sObjectList);
		}
	}
}