public with sharing class ObjectPlural extends fflib_SObjects
	implements IObjectPlural
{
	public static IObjectPlural newInstance(List<objectAPIname> recordList)
	{
		return (IObjectPlural) Application.Domain.newInstance(recordList);
	}
	
	public static IObjectPlural newInstance(Set<Id> recordIdSet)
	{
		return (IObjectPlural) Application.Domain.newInstance(recordIdSet);
	}
	
	public ObjectPlural(List<objectAPIname> sObjectList)
	{
		super(sObjectList, Schema.objectAPIname.SObjectType);
	}

	public List<objectAPIname> getObjectPlural()
	{
		return (List<objectAPIname>) getRecords();
	}

	public class Constructor implements fflib_IDomainConstructor
	{
		public fflib_SObjects construct(List<Object> objectList)
		{
			return new ObjectPlural((List<SObject>) objectList);
		}
	}
}