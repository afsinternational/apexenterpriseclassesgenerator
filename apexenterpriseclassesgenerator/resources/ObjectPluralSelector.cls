public class ObjectPluralSelector extends fflib_SObjectSelector
	implements IObjectPluralSelector
{
	public static IObjectPluralSelector newInstance()
	{
		return (IObjectPluralSelector) Application.Selector.newInstance(objectAPIname.SObjectType);
	}
	
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			objectAPIname.Id,
			objectAPIname.Name
		};
	}

	public Schema.SObjectType getSObjectType()
	{
		return objectAPIname.SObjectType;
	}

	public List<objectAPIname> selectById(Set<Id> idSet)
	{
		return (List<objectAPIname>) selectSObjectsById(idSet);
	}
}