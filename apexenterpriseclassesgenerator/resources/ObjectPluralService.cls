public with sharing class ObjectPluralService
{
	private static IObjectPluralService service()
	{
		return (IObjectPluralService) Application.Service.newInstance(IObjectPluralService.class);
	}	
}