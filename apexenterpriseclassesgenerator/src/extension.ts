import * as vscode from 'vscode';
import * as fstat from 'fs';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    let generator = vscode.commands.registerCommand('apexenterpriseclassesgenerator.generateClasses', async () => {
		const objectAPIname = await vscode.window.showInputBox({
			title: 'Enter the API name of object!',
			ignoreFocusOut: true,
			placeHolder: 'MyObject__c',
		});

		const objectPluralName = await vscode.window.showInputBox({
			title: 'Enter the plural name of the object!',
			ignoreFocusOut: true,
			placeHolder: 'MyObjects',
		});

		if(objectAPIname !== undefined && objectPluralName !== undefined){
			var workspace = vscode.workspace.workspaceFolders;
			if(workspace !== undefined){
				vscode.window.showInformationMessage('Generation starts now...');
				var objectparams = new ObjectParameters(objectAPIname, objectPluralName);
				const filesToCreate = getFiles();
				filesToCreate.forEach(params => {
					copyFile(context, objectparams, params.fileName, params.path);					
				});				
				vscode.window.showInformationMessage('Generation finsihed!');
			}
			else{
				vscode.window.showInformationMessage('No workspace open.');
			}
		}
		else{
			vscode.window.showErrorMessage('Not correct object names!');
		}
	});

	context.subscriptions.push(generator);
}

/**
 * @param {vscode.ExtensionContext} context extensioncontext
 * @param {ObjectParameters} objectParameters parameters of the object
 * @param {string} filename name of the file
 * @param {string} targetpath path to copy the file
 */
 async function copyFile(context : vscode.ExtensionContext, objectParameters : ObjectParameters, filename : string, targetpath : string)
 {
     if((filename !== null || filename !== undefined) && (context !== null || context !== undefined) && (targetpath !== null || targetpath !== undefined)){
        const file = vscode.Uri.file(context.asAbsolutePath('resources/' + filename));
        filename = filename.replace('ObjectPlural', objectParameters.objectPlural);
        if(vscode.workspace.workspaceFolders !== undefined) {
            const folderUri = vscode.workspace.workspaceFolders[0].uri;
            const fileUri = vscode.Uri.joinPath(folderUri,targetpath,filename);
            const fileExistsAlready = fstat.existsSync(fileUri.fsPath);
    
            if(!fileExistsAlready){
                var data = (await vscode.workspace.fs.readFile(file)).toString();
                data = data.replace(/ObjectPlural/g, objectParameters.objectPlural);
                data = data.replace(/objectAPIname/g, objectParameters.objectAPIname);
                await vscode.workspace.fs.writeFile(fileUri, Buffer.from(data));
            }
        }
    }         
 }
 
 function getFiles(){
     return [
         //selectors
         new FileParamters('ObjectPluralSelector.cls','/force-app/main/default/classes/enterprise/selectors/classes/'),
         new FileParamters('ObjectPluralSelector.cls-meta.xml','/force-app/main/default/classes/enterprise/selectors/classes/'),
         new FileParamters('IObjectPluralSelector.cls','/force-app/main/default/classes/enterprise/selectors/interfaces/'),
         new FileParamters('IObjectPluralSelector.cls-meta.xml','/force-app/main/default/classes/enterprise/selectors/interfaces/'),
 
         //domains
         new FileParamters('ObjectPlural.cls','/force-app/main/default/classes/enterprise/domains/classes/'),
         new FileParamters('ObjectPlural.cls-meta.xml','/force-app/main/default/classes/enterprise/domains/classes/'),
         new FileParamters('IObjectPlural.cls','/force-app/main/default/classes/enterprise/domains/interfaces/'),
         new FileParamters('IObjectPlural.cls-meta.xml','/force-app/main/default/classes/enterprise/domains/interfaces/'),
 
         //services
         new FileParamters('ObjectPluralService.cls','/force-app/main/default/classes/enterprise/services/classes/'),
         new FileParamters('ObjectPluralService.cls-meta.xml','/force-app/main/default/classes/enterprise/services/classes/'),
         new FileParamters('ObjectPluralServiceImpl.cls','/force-app/main/default/classes/enterprise/services/classes/'),
         new FileParamters('ObjectPluralServiceImpl.cls-meta.xml','/force-app/main/default/classes/enterprise/services/classes/'),
         new FileParamters('IObjectPluralService.cls','/force-app/main/default/classes/enterprise/services/interfaces/'),
         new FileParamters('IObjectPluralService.cls-meta.xml','/force-app/main/default/classes/enterprise/services/interfaces/'),
 
         //triggers
         new FileParamters('ObjectPlural.trigger','/force-app/main/default/triggers/enterprise/'),
         new FileParamters('ObjectPlural.trigger-meta.xml','/force-app/main/default/triggers/enterprise/'),
         new FileParamters('ObjectPluralTriggerHandler.cls','/force-app/main/default/classes/enterprise/triggerHandlers/'),
         new FileParamters('ObjectPluralTriggerHandler.cls-meta.xml','/force-app/main/default/classes/enterprise/triggerHandlers/'),
 
         //tests
         new FileParamters('ObjectPluralTest.cls','/force-app/test/default/classes/enterprise/domains/'),
         new FileParamters('ObjectPluralTest.cls-meta.xml','/force-app/test/default/classes/enterprise/domains/'),
         new FileParamters('ObjectPluralServiceTest.cls','/force-app/test/default/classes/enterprise/services/'),
         new FileParamters('ObjectPluralServiceTest.cls-meta.xml','/force-app/test/default/classes/enterprise/services/'),
     ];
 }
 
 class ObjectParameters {
    objectAPIname : string;
    objectPlural : string;

     constructor(objectAPIname : string, objectPlural : string) {
         this.objectAPIname = objectAPIname;
         this.objectPlural = objectPlural;
       }
 }

 class FileParamters {
    fileName : string;
    path : string;

	constructor(filename : string, pathname : string){
		this.fileName = filename;
		this.path = pathname;
	}
}